main(); 
window.onload = function () {
	showClientDetails(); 
};
// 
const listItems = document.querySelectorAll(".sidebar-list li");
listItems.forEach((item) => {
	item.addEventListener("click", () => {
		let isActive = item.classList.contains("active");

		listItems.forEach((el) => {
			el.classList.remove("active");
		});

		if (isActive) item.classList.remove("active");
		else item.classList.add("active");
	});
});

const toggleSidebar = document.querySelector(".toggle-sidebar");
const logo = document.querySelector(".logo-box");
const sidebar = document.querySelector(".sidebar");

toggleSidebar.addEventListener("click", () => {
	sidebar.classList.toggle("close");
});

logo.addEventListener("click", () => {
	sidebar.classList.toggle("close");
});

var clientsData;
 
function abc() {
	fetch("../../getdata.php")
	.then((res) => res.json())
	.then((data) => {
		clientsData = data; 
	})
}   
// 
function main() {
	// Get data
	fetch("../../getdata.php")
		.then((res) => res.json())
		.then((data) => {
			// Get data  
			const { clients, client_config } = data;
			//  
			var renderClients = clients.map(client =>
				renderHtml(client, client_config)
			);
			document.getElementById("items-list").innerHTML = renderClients.join('');
			// Search container
			var txtSearch = document.getElementById("txt-search");
			var searchValue = "";
			var clientsData = [];
			txtSearch.addEventListener('keyup',
				function () {
					searchValue = txtSearch.value;
					if (searchValue.length > 0) {
						clientsData = clients.filter((word) => word.includes(searchValue));
						// 
						renderClients = clientsData.map(client =>
							renderHtml(client, client_config)
						);
					} else {
						renderClients = clients.map(client =>
							renderHtml(client, client_config)
						);
					}
					document.getElementById("items-list").innerHTML = renderClients.join('');
					showClientDetails();
				}
			); 
	});

	// Add client
	var btnAddClient = document.getElementById("btn-add-client"); 
	btnAddClient.addEventListener("click", () => {
		var newClientName = document.getElementById("txt-add-client").value;
		var clientsExistArr;
		fetch("../../getClientsnumber.php")
			.then((response) => response.text())
			.then((data) => {
				// Get data
				clientsExistArr = data.trim().split('\n');
				var clientExist = clientsExistArr.includes(newClientName);
				if(newClientName.length == 0 || newClientName == '' || newClientName == null) {
					Toastify({
						text: "The name must not be empty !",
						className: "toastify-warning" 
						}).showToast();
				} else if(clientExist) {
					Toastify({
						text: "The name is exist !",
						className: "toastify-warning"
					}).showToast();
				} else {
					const newClient = {
						name: newClientName
					}
					fetch("../../postAddClient.php", {
						method: "POST", // *GET, POST, PUT, DELETE, etc.
						headers: {
							"Content-Type": "application/json; charset=utf-8"
						},
						body: JSON.stringify(newClient) 
					})
					.then((response) => response.json())
					.then((data) => {
						if(data.success == 0) {
							Toastify({
								text: "Add Client successfully !",
								className: "toastify-success", 
							}).showToast();
						}
					})
					window.location.reload();
					
				}
			})
	});
}


function showClientDetails() {
	const infoItems = document.querySelectorAll(".items-list .item-info");
	infoItems.forEach((item) => {
		item.addEventListener("click", () => {
			let infoItem = item.classList.contains("item-info-active");

			infoItems.forEach((el) => {
				el.classList.remove("item-info-active");
			});

			if (infoItem) item.classList.remove("item-info-active");
			else item.classList.add("item-info-active");
		});
	});
}



function renderHtml(x, y) {
	var innerhtml =
	`<li class="item">
		<div class="item-title">
			<span class="material-icons">devices</span>
			<span class="item-name">${x}</span>
		</div>
		<div class="btn-container"> 
			<span class="material-icons btn-remove">power_settings_new</span>
			<form action="../../getProfile.php" method="post">
				<input type="hidden" name="clientName[]" value="${x}">
				<button type="submit" name="btnForm" class="btn-form"><span class="material-icons btn-download">file_download</span></button>
			</form>
			<div class="item-info">
				<span class="material-icons btn-info">priority_high</span>
				<div class="item-details">
					<ul>
						<li>
							<span class="name-detail address">Address: </span>
							<span class="address-value">${y[x]['Address']}</span>
						</li>
						<li>
							<span class="name-detail privatekey">PrivateKey: </span>
							<p class="privatekey-value">${y[x]['PrivateKey']}</p>
						</li>
						<li>
							<span class="name-detail publickey">PublicKey: </span>
							<p class="publickey-value">${y[x]['PublicKey']}</p>
						</li>
						<li>
							<span class="name-detail endpoint">Endpoint: </span>
							<span class="endpoint-value">${y[x]['Endpoint']}</span>
						</li>
						<li>
							<span class="name-detail allowedips">AllowedIPs: </span>
							<span class="allowedips-value">${y[x]['AllowedIPs']}</span>
						</li>
						<li>
							<span class="name-detail keepalive">Persistentkeepalive: </span>
							<span class="keepalive-value">${y[x]['Persistentkeepalive']}</span>
						</li>
					</ul>
				</div>
			</div>
		</div> 
	</li>`
	return innerhtml
}
