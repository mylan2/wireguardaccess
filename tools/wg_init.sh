#!/bin/bash

# Define paths and variables
WG_CONFIG_PATH="/etc/wireguard"
WG_FILE_CONF="wg2"
SERVER_PORT="51820"
# Function to generate a new client configuration
generate_client_config() {
    CLIENT_NAME=$1
    # Generate client key pair
    CLIENT_PRIVATE_KEY=$(wg genkey)
    CLIENT_PUBLIC_KEY=$(echo "$CLIENT_PRIVATE_KEY" | wg pubkey)

    # Generate client configuration file
    CLIENT_CONF="$WG_CONFIG_PATH/clients/$CLIENT_NAME/wg0.conf"
    mkdir -p "$WG_CONFIG_PATH/clients/$CLIENT_NAME"
    touch "$CLIENT_CONF"
    chmod 600 "$CLIENT_CONF"

    wg genkey | tee "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPrivateKey" | wg pubkey > "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPublicKey"

    CLIENT_PUBLIC_KEY=$(cat "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPublicKey")
    CLIENT_PRIVATE_KEY=$(cat "$WG_CONFIG_PATH/clients/$CLIENT_NAME/ClientPrivateKey")
    CLIENT_ADDRESS="10.10.0.$((10 + $(ls -1 "$WG_CONFIG_PATH/clients" | wc -l)))"

    echo "[Interface]" > "$CLIENT_CONF"
    echo "Address = $CLIENT_ADDRESS" >> "$CLIENT_CONF"
    echo "PrivateKey = $CLIENT_PRIVATE_KEY" >> "$CLIENT_CONF"
    echo "[Peer]" >> "$CLIENT_CONF"
    echo "PublicKey = $SERVER_PUBLIC_KEY" >> "$CLIENT_CONF"
    echo "Endpoint = $SERVER_PUBLIC_IP:$SERVER_PORT" >> "$CLIENT_CONF"
    echo "AllowedIPs = 10.10.0.0/24" >> "$CLIENT_CONF"
    echo "PersistentKeepalive = 15" >> "$CLIENT_CONF"

    # Update server
    echo "" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "[Peer]" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PublicKey = $CLIENT_PUBLIC_KEY"  >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "AllowedIPs = $CLIENT_ADDRESS/32" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    
    wg set $WG_FILE_CONF peer $CLIENT_PUBLIC_KEY allowed-ips $CLIENT_ADDRESS

    # # Restart server
    # systemctl restart wg-quick@$WG_FILE_CONF.service
}

# Function to initialize WireGuard server
initialize_wireguard() {
    # get NIC
    NETWORK_INTERFACE_CARD=$(ip -o -4 route show to default | awk '{print $5}')
    # Initialize WireGuard interface
    wg genkey | tee "$WG_CONFIG_PATH/ServerPrivateKey" | wg pubkey > "$WG_CONFIG_PATH/ServerPublicKey"
    chmod 600 "$WG_CONFIG_PATH/ServerPrivateKey"
    chmod 600 "$WG_CONFIG_PATH/ServerPublicKey"

    # Set up server configuration
    echo "[Interface]" > "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "Address = 10.10.0.1/24" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "ListenPort = $SERVER_PORT" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PostUp = iptables -A FORWARD -i $WG_FILE_CONF -j ACCEPT; iptables -t nat -A POSTROUTING -o $NETWORK_INTERFACE_CARD -j MASQUERADE" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PostDown = iptables -D FORWARD -i $WG_FILE_CONF -j ACCEPT; iptables -t nat -D POSTROUTING -o $NETWORK_INTERFACE_CARD -j MASQUERADE" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"
    echo "PrivateKey = $(cat "$WG_CONFIG_PATH/ServerPrivateKey")" >> "$WG_CONFIG_PATH/$WG_FILE_CONF.conf"

    # Run server interface
    systemctl enable wg-quick@$WG_FILE_CONF
    systemctl start wg-quick@$WG_FILE_CONF
}

# Main script

# Check if WireGuard is installed
if ! command -v wg &> /dev/null; then
    # echo "WireGuard is not installed. Please install WireGuard first."
    exit 1
fi

# Check if running as root
if [ "$EUID" -ne 0 ]; then
    # echo "Please run this script as root or with sudo."
    exit 1
fi

# Check if WireGuard is already initialized
if ! [ -f "$WG_CONFIG_PATH/$WG_FILE_CONF.conf" ]; then
    initialize_wireguard
fi

# Get server public key and public IP
SERVER_PUBLIC_KEY=$(cat "$WG_CONFIG_PATH/ServerPublicKey")
SERVER_PUBLIC_IP="192.168.188.141"

# Allow adding clients
NAME=$1
generate_client_config "$NAME" 


 
