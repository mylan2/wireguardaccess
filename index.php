<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> </title>

    <!-- Box Icons  -->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <!-- Material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <!-- Styles  --> 
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Script -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body>
    <div class="sidebar close">
        <!-- ========== Logo ============  -->
        <a href="#" class="logo-box">
            <img src="assets/img/logo1.jpg" alt="">
            <div class="logo-name">Wireguard Access</div>
        </a>
   
        <!-- ========== List ============  -->
        <ul class="sidebar-list">
            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-grid-alt'></i>
                        <span class="name">Dashboard</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Dashboard</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Dropdown List Item ------- -->
            <li class="dropdown">
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-collection'></i>
                        <span class="name">Category</span>
                    </a>
                    <i class='bx bxs-chevron-down'></i>
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Category</a>
                    <a href="#" class="link">HTML & CSS</a>
                    <a href="#" class="link">JavaScript</a>
                    <a href="#" class="link">PHP & MySQL</a>
                </div>
            </li>

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-line-chart'></i>
                        <span class="name">Analytics</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Analytics</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-pie-chart-alt-2'></i>
                        <span class="name">Chart</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Chart</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Dropdown List Item ------- -->
            <li class="dropdown">
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-extension'></i>
                        <span class="name">Plugins</span>
                    </a>
                    <i class='bx bxs-chevron-down'></i>
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Plugins</a>
                    <a href="#" class="link">UI Face</a>
                    <a href="#" class="link">Pigments</a>
                    <a href="#" class="link">Box Icons</a>
                </div>
            </li>

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-compass'></i>
                        <span class="name">Explore</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Explore</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-history'></i>
                        <span class="name">History</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">History</a>
                    <!-- submenu links here  -->
                </div>
            </li>

            <!-- -------- Non Dropdown List Item ------- -->
            <li>
                <div class="title">
                    <a href="#" class="link">
                        <i class='bx bx-cog'></i>
                        <span class="name">Settings</span>
                    </a>
                    <!-- <i class='bx bxs-chevron-down'></i> -->
                </div>
                <div class="submenu">
                    <a href="#" class="link submenu-title">Settings</a>
                    <!-- submenu links here  -->
                </div>
            </li>
        </ul>
    </div>
    <!-- ============= Home Section =============== -->
    <section class="home">
        <div class="toggle-sidebar">
            <i class='bx bx-menu'></i>
        </div>
        <!-- Main -->
        <div class="container">
            <div class="tools-bar"> 
                <div class="search-tool tools">
                    <label for="search"><small>Search:</small></label>
                    <input type="" class="txt-search" id="txt-search" placeholder="Search">
                </div> 
                <div class="add-tool tools">
                    <label for="add"><small>Add:</small></label>
                    <div style="display: inline">
                        <input type="" class="txt-search" id="txt-add-client" placeholder="Enter name">
                        <button class="btn btn-blue" id="btn-add-client">Add Client</button>
                    </div>
                 </div>
            </div>
            
            <div class="content">
                <div class="loading-page" id="loading-page"> 
                    <img class="loader" src="./assets/img/logo1.jpg" alt=""> 
                </div>
                <ul class="items-list" id="items-list">
                <!-- <?php 
                    shell_exec('python3 /opt/wireguard/script.py 2>&1');
                    $data = shell_exec('python3 /opt/wireguard/script.py 2>&1');
                    $data = json_decode($data); 
                    // var_dump($data);
                    $clients = $data->clients; 
                    $clients_config = $data->client_config;
                    $a = $clients_config->client1;

                    if(empty($clients)) {
                        echo "<p>No Data </p>";
                    } else {
                        foreach ($clients as $client) { 
                            $client_config_data = $clients_config->$client; 
                            ?> 
                            <li class="item">
                                <div class="item-title">
                                    <span class="material-icons">devices</span>
                                    <span class="item-name"><?= $client ?></span>  
                                </div>
                                <span class="material-icons btn-del">remove</span>
                                <div class="item-info">  
                                    <span class="material-icons btn-info">priority_high</span>
                                    
                                    <div class="item-details">
                                        <ul>
                                            <li>
                                                <span class="name-detail address">Address: </span>
                                                <span class="address-value"><?= $client_config_data->Address ?></span>
                                            </li>
                                            <li>
                                                <span class="name-detail privatekey">Privatekey: </span>
                                                <p class="privatekey-value"><?= $client_config_data->Privatekey ?></p>
                                            </li>
                                            <li>
                                                <span class="name-detail publickey">PublicKey: </span>
                                                <p class="publickey-value"><?= $client_config_data->PublicKey ?></p>
                                            </li>
                                            <li>
                                                <span class="name-detail endpoint">Endpoint: </span>
                                                <span class="endpoint-value"><?= $client_config_data->Endpoint ?></span>
                                            </li>
                                            <li>
                                                <span class="name-detail allowedips">AllowedIPs: </span>
                                                <span class="allowedips-value"><?= $client_config_data->AllowedIPs ?></span>
                                            </li>
                                            <li>
                                                <span class="name-detail keepalive">Persistentkeepalive: </span>
                                                <span class="keepalive-value"><?= $client_config_data->Persistentkeepalive ?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>   
                        <?php }
                    }
 
                ?> -->
                </ul>
                
            </div>
        </div>
    </section>
     
     
    <!-- Link JS -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    <script src="assets/js/main.js"></script>
    
</body>

</html>